//
//  Created by Fran Ruano on 31/1/21.
//

import XCTest
@testable import Citibox_V2

class RouterTests: XCTestCase {

    var router: Router!
    
    override func setUpWithError() throws {
       
        router = Router()
        
    }

    override func tearDownWithError() throws {
        
        router = nil
    }

    func testGetInitialNavController() throws {
        
        let stack = router.getInitialNavController()
        XCTAssertTrue(stack === router.navigationStack)
        XCTAssertNotNil(stack)
        XCTAssertEqual(stack?.viewControllers.count, 1)
        XCTAssertNotNil(stack?.viewControllers.first as? PostListViewController)
        
        let view = stack!.viewControllers.first as! PostListViewController
        XCTAssertNotNil(view.presenter)
    }
    
    func testMoveToPost() {
        let myPredicate = NSPredicate { input, _ in
                return (input as? UINavigationController)?.topViewController is PostDetailsViewController
            }

        let stack = router.getInitialNavController()
        expectation(for: myPredicate, evaluatedWith: stack)
        router.moveToPost(Post(1))
        waitForExpectations(timeout: 2)
        
        XCTAssertTrue(stack === router.navigationStack)
    
        XCTAssertEqual(stack?.viewControllers.count, 2)
        XCTAssertNotNil(stack?.topViewController as? PostDetailsViewController)
        
        let view = stack?.topViewController as? PostDetailsViewController
        XCTAssertNotNil(view?.presenter)
        
        
    }
    
    func testMoveToComments() {
        let myPredicate = NSPredicate { input, _ in
                return (input as? UINavigationController)?.topViewController is PostCommentsViewController
            }
        
        let stack = router.getInitialNavController()
        expectation(for: myPredicate, evaluatedWith: stack)
        router.moveToComments([Comment]())
        waitForExpectations(timeout: 2)
        XCTAssertTrue(stack === router.navigationStack)
        
        XCTAssertEqual(stack?.viewControllers.count, 2)
        XCTAssertNotNil(stack?.topViewController as? PostCommentsViewController)
        
        let view = stack?.topViewController as? PostCommentsViewController
        XCTAssertNotNil(view?.presenter)
    }
}
