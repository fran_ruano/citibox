//
//  Created by Fran Ruano on 21/1/21.
//

import XCTest
@testable import Citibox_V2
import Alamofire
import RealmSwift

class MockPostListCacheService: PostListCacheServiceProtocol {
    var storage: Realm
    
    required init(_ storage: Realm) {
        self.storage = storage
    }
    
    
}

class MockPostListNetworkService: PostListNetworkServiceProtocol {
    required init(_ interactor: PostListInteractorProtocol, handler: Session) {
        
    }
    
    func requestPosts(page: Int) {
        
    }
}

class MockHandler: Alamofire.Session {
    
}


class PostListInteractorTests: XCTestCase {

    var mockPostListCacheService: MockPostListCacheService!
    var mockPostListNetworkService: MockPostListNetworkService!
    var mockPresenter: MockPostListPresenter!
    var interactor: PostListInteractor!
    var mockHandler: MockHandler!
    var mockRouter: MockRouter!
    
    override func setUpWithError() throws {
        interactor = PostListInteractor()
        mockPostListCacheService = MockPostListCacheService(try! Realm())
        mockHandler = MockHandler()
        mockPostListNetworkService = MockPostListNetworkService(interactor, handler: mockHandler)
        mockRouter = MockRouter()
        mockPresenter = MockPostListPresenter(mockRouter)
        
    }

    override func tearDownWithError() throws {
        
        mockRouter = nil
        mockHandler = nil
        mockPostListCacheService = nil
        mockPostListNetworkService = nil
        mockPresenter = nil
        interactor = nil
    }

    func testSetNetworkService() throws {
        
        interactor.setNetworkService(mockPostListNetworkService)
        XCTAssertNotNil(interactor.serviceNetwork)
        XCTAssertTrue(interactor.serviceNetwork === mockPostListNetworkService)
    }
    
    func testSetCacheService() throws {
        
        interactor.setCacheService(mockPostListCacheService)
        XCTAssertNotNil(interactor.serviceCache)
        XCTAssertTrue(interactor.serviceCache === mockPostListCacheService)
    }
    
    func testSetPresenter() throws {
        
        interactor.setPresenter(mockPresenter)
        XCTAssertNotNil(interactor.presenter)
        XCTAssertTrue(interactor.presenter === mockPresenter)
    }
}
