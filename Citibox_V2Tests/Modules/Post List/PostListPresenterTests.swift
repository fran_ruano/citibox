//
//  Created by Fran Ruano on 21/1/21.
//

import XCTest
@testable import Citibox_V2

class PostListPresenterTests: XCTestCase {

    var mockPostListInteractor: MockPostListInteractor!
    var mockPostListView: MockPostListView!
    var presenter: PostListPresenter!
    var mockRouter: MockRouter!
    
    override func setUpWithError() throws {
        mockRouter = MockRouter()
        mockPostListInteractor = MockPostListInteractor()
        mockPostListView = MockPostListView()
        presenter = PostListPresenter(mockRouter)
    }

    override func tearDownWithError() throws {
        mockRouter = nil
        mockPostListInteractor = nil
        mockPostListView = nil
        presenter = nil
    }

    func testInit() throws {
        XCTAssertNotNil(presenter)
        XCTAssertNil(presenter.view)
        XCTAssertNil(presenter.interactor)
    }
    
    func testSetView() {
        presenter.setView(mockPostListView)
        XCTAssertNotNil(presenter.view)
        XCTAssertTrue(presenter.view === mockPostListView)
    }
    
    func testSetInteractor() {
        presenter.setInteractor(mockPostListInteractor)
        XCTAssertNotNil(presenter.interactor)
        XCTAssertTrue(presenter.interactor === mockPostListInteractor)
    }
}
