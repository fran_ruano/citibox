//
//  Created by Fran Ruano on 21/1/21.
//

import XCTest
@testable import Citibox_V2
import RealmSwift

class PostListCacheServiceTests: XCTestCase {

    var service: PostListCacheService!
    var mockInteractor: MockPostListInteractor!
    var realm: Realm!
    
    override func setUpWithError() throws {
    
        realm = try! Realm()
        service = PostListCacheService(realm)
    }

    override func tearDownWithError() throws {
        try realm.write {
            realm.deleteAll()
        }
        service = nil
        realm = nil
    }
    
    func testInit() {
        XCTAssertNotNil(service.storage)
    }
    
    func testSaveOneObject() {
        let post = Post(1)
        service.save(post)
        
        let list = realm.objects(Post.self)
        XCTAssertEqual(list.count, 1)
    }

    func testSaveMultipleObjects() {
        let post1 = Post(1)
        let post2 = Post(2)
        let post3 = Post(3)
        service.save([post1, post2, post3])
        
        let list = realm.objects(Post.self)
        XCTAssertEqual(list.count, 3)
    }
    
    func testGetObjects() {
        let post1 = Post(1)
        let post2 = Post(2)
        let post3 = Post(3)
        service.save([post1, post2, post3])
        
        let list = service.getObjects(Post.self)
        XCTAssertEqual(list.count, 3)
    }
    
    func testDeleteAll() {
        service.deleteAll()
        let post1 = Post(1)
        let post2 = Post(2)
        let post3 = Post(3)
        service.save([post1, post2, post3])
        
        let list = service.getObjects(Post.self)
        XCTAssertEqual(list.count, 3)
        service.deleteAll()
        
        let listEmpty = service.getObjects(Post.self)
        XCTAssertEqual(listEmpty.count, 0)
    }
}
