//
//  Created by Fran Ruano on 21/1/21.
//

import Foundation
@testable import Citibox_V2

class MockPostListPresenter: PostListPresenterProtocol {
    func updatePage(_ postCount: Int) {
        
    }
    
    required init(_ router: Router) {
        
    }
    
    func selectRow(_ index: Int) {
        
    }
    
    func requestPosts(reload: Bool) {
        
    }
    
    func responsePosts(_ posts: [Post]) {
        
    }

    func setView(_ view: PostListViewProtocol) {
        
    }
    
    func setInteractor(_ interactor: PostListInteractorProtocol) {
        
    }
    
    
}
