//
//  Created by Fran Ruano on 21/1/21.
//

import Foundation
@testable import Citibox_V2

class MockPostListView: PostListViewProtocol {
    func responsePosts(_ posts: [PostViewItem], reload: Bool) {
        
    }
    
    func setPresenter(_ presenter: PostListPresenterProtocol) {
    }
}
