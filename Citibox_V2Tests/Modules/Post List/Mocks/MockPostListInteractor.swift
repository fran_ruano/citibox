//
//  Created by Fran Ruano on 21/1/21.
//

import Foundation
@testable import Citibox_V2

class MockPostListInteractor: PostListInteractorProtocol {
    func forceRequestPost(page: Int) {
        
    }
    
    func requestPosts(page: Int) {
        
    }
    
    var requestPostsCalled = [Result<[JsonPost], Error>]()
    func responsePosts(_ response: Result<[JsonPost], Error>) {
        requestPostsCalled.append(response)
    }
    
    func setNetworkService(_ service: PostListNetworkServiceProtocol) {
        
    }
    
    func setCacheService(_ service: PostListCacheServiceProtocol) {
        
    }
    
    func setPresenter(_ presenter: PostListPresenterProtocol) {
        
    }
}
