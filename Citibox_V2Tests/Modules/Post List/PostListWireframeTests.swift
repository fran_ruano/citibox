//
//  Created by Fran Ruano on 19/1/21.
//
@testable import Citibox_V2

import XCTest

class PostListWireframeTests: XCTestCase {

    var view: PostListViewController!
    var mockRouter: MockRouter!
    
    override func setUpWithError() throws {
        view = PostListViewController()
        mockRouter = MockRouter()
    }

    override func tearDownWithError() throws {
        view =  nil
        mockRouter = nil
    }

    func testPrepare() throws {
        
        PostListWireframe.preparePostListView(view, router: mockRouter)
        
        XCTAssertNotNil(view)
        XCTAssertNotNil(view.presenter)
        let presenter = view.presenter as! PostListPresenter
        XCTAssertNotNil(presenter.interactor)
        let interactor = presenter.interactor as! PostListInteractor
        XCTAssertNotNil(interactor.serviceCache)
        XCTAssertNotNil(interactor.serviceNetwork)
    }
}
