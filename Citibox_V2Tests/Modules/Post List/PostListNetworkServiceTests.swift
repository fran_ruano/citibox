//
//  Created by Fran Ruano on 21/1/21.
//

import XCTest
@testable import Citibox_V2
import Alamofire
import OHHTTPStubs

class PostListNetworkServiceTests: XCTestCase {

    var service: PostListNetworkService!
    var mockInteractor: MockPostListInteractor!
    let testHost = "jsonplaceholder.typicode.com"
        
    override func setUpWithError() throws {
        mockInteractor = MockPostListInteractor()
        service = PostListNetworkService(mockInteractor, handler: Alamofire.AF)
        
        HTTPStubs.onStubActivation { (request: URLRequest, stub: HTTPStubsDescriptor, response: HTTPStubsResponse) in
                    print("*** [OHHTTPStubs] Request to \(request.url!) has been stubbed with \(String(describing: stub.name)) ***")
                }
    }

    override func tearDownWithError() throws {

        mockInteractor = nil
        service = nil
        HTTPStubs.removeAllStubs()
    }

    func testRequestPostsSuccess() {
        
        stub(condition: isHost(testHost) && isPath("/posts") && isMethodGET()) { req in
                guard let path = OHPathForFile("response_posts_20.json", type(of: self)) else {
                        preconditionFailure("Could not find expected file in test bundle")
                }
                return fixture(filePath: path,
                               status: 200,
                               headers: [ "Content-Type": "application/json" ])
            }
        
        let predicate = NSPredicate(block: { [self] any, _ in
            return mockInteractor.requestPostsCalled.count > 0
          })
        
        let exp = expectation(for: predicate, evaluatedWith: self, handler: nil)

        service.requestPosts(page: 1)

        wait(for: [exp], timeout: 10.0)
        XCTAssertEqual(mockInteractor.requestPostsCalled.count, 1)
        if case let .success(data) = mockInteractor.requestPostsCalled.first {
            XCTAssertEqual(data.count, 9)
        } else  {
            XCTFail()
        }

    }
    
    func testRequestPostsError() {
        
        stub(condition: isHost(testHost) && isPath("/posts") && isMethodGET()) { req in
                
            return HTTPStubsResponse(error: NetworkError.badURL)
            }
        
        let predicate = NSPredicate(block: { [self] any, _ in
            return mockInteractor.requestPostsCalled.count > 0
          })
        
        let exp = expectation(for: predicate, evaluatedWith: self, handler: nil)

        service.requestPosts(page: 1)

        wait(for: [exp], timeout: 10.0)
        XCTAssertEqual(mockInteractor.requestPostsCalled.count, 1)
        if case let .failure(error) = mockInteractor.requestPostsCalled.first {
            XCTAssertNotNil(error)
        } else  {
            XCTFail()
        }

    }
}
