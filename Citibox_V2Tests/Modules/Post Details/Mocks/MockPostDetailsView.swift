//
//  Created by Fran Ruano on 21/1/21.
//

import Foundation
@testable import Citibox_V2

class MockPostDetailsView: PostDetailsViewProtocol {
    func setComments(_ name: String) {
        
    }
    
    func setItem(_ item: PostViewItem) {
        
    }
    
    func setUserName(_ name: String) {
        
    }

    func setPresenter(_ presenter: PostDetailsPresenterProtocol) {
    }
}
