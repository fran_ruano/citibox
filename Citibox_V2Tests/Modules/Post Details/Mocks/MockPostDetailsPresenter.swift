//
//  Created by Fran Ruano on 21/1/21.
//

import Foundation
@testable import Citibox_V2

class MockPostDetailsPresenter: PostDetailsPresenterProtocol {
    func openComments() {
        
    }
    
    func setInteractor(_ interactor: PostCommentsInteractorProtocol) {
        
    }
    
    func requestDetails() {
        
    }
    
    func responseUser(_ user: User) {
        
    }
    
    required init(post: Post, router: Router) {
        
    }
    
    func setView(_ view: PostDetailsViewProtocol) {
        
    }
    
    func setInteractor(_ interactor: PostDetailsInteractorProtocol) {
        
    }
    
    
}
