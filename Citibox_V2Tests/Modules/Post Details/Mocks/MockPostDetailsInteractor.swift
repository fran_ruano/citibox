//
//  Created by Fran Ruano on 21/1/21.
//

import Foundation
@testable import Citibox_V2

class MockPostDetailsInteractor: PostDetailsInteractorProtocol {
    var requestDetailsCalled = [Int]()
    func requestDetails(id: Int) {
        
        requestDetailsCalled.append(id)
    }
    
    var responseDetailsCalled = [Result<JsonUser, Error>]()
    func responseDetails(_ response: Result<JsonUser, Error>) {
        responseDetailsCalled.append(response)
    }
    
    func setNetworkService(_ service: PostDetailsNetworkServiceProtocol) {
        
    }
    
    func setCacheService(_ service: PostDetailsCacheServiceProtocol) {
        
    }
    
    func setPresenter(_ presenter: PostDetailsPresenterProtocol) {
        
    }
}
