//
//  Created by Fran Ruano on 21/1/21.
//

import XCTest
@testable import Citibox_V2

class MockRouter: Router {
    
}

class PostDetailsPresenterTests: XCTestCase {

    var mockRouter: MockRouter!
    var mockPostDetailsInteractor: MockPostDetailsInteractor!
    var mockPostDetailsView: MockPostDetailsView!
    var presenter: PostDetailsPresenter!
    var post: Post!
    
    override func setUpWithError() throws {
        post = Post(1)
        mockRouter = MockRouter()
        mockPostDetailsInteractor = MockPostDetailsInteractor()
        mockPostDetailsView = MockPostDetailsView()
        presenter = PostDetailsPresenter(post: post, router: mockRouter)
    }

    override func tearDownWithError() throws {
        
        mockRouter = nil
        mockPostDetailsInteractor = nil
        mockPostDetailsView = nil
        presenter = nil
    }

    func testInit() throws {
//        XCTAssertNotNil(presenter)
//        XCTAssertNil(presenter.view)
//        XCTAssertNil(presenter.interactor)
    }
    
    func testSetView() {
//        presenter.setView(mockPostDetailsView)
//        XCTAssertNotNil(presenter.view)
//        XCTAssertTrue(presenter.view === mockPostDetailsView)
    }
    
    func testSetInteractor() {
//        presenter.setInteractor(mockPostDetailsInteractor)
//        XCTAssertNotNil(presenter.interactor)
//        XCTAssertTrue(presenter.interactor === mockPostDetailsInteractor)
    }
}
