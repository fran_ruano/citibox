//
//  Created by Fran Ruano on 21/1/21.
//

import XCTest
@testable import Citibox_V2
import Alamofire
import RealmSwift

class MockPostDetailsCacheService: PostDetailsCacheServiceProtocol {
    func getUserById(_ id: Int) -> Citibox_V2.User? {
        
        return nil
    }
    
    var storage: Realm
    
    required init(_ storage: Realm) {
        self.storage = storage
    }
    
    
}

class MockPostDetailsNetworkService: PostDetailsNetworkServiceProtocol {
    required init(_ interactor: PostDetailsInteractorProtocol, handler: Session) {
        
    }
    
    func requestDetails(id: Int) {
        
    }
    
    
}

class PostDetailsInteractorTests: XCTestCase {

    var mockPostDetailsCacheService: MockPostDetailsCacheService!
    var mockPostDetailsNetworkService: MockPostDetailsNetworkService!
    var mockPresenter: MockPostDetailsPresenter!
    var interactor: PostDetailsInteractor!
    var post: Post!
    var mockRouter: MockRouter!
    var mockInteractor: MockPostDetailsInteractor!
    var mockHanlder: MockHandler!
    
    override func setUpWithError() throws {
        post = Post(1)
        mockRouter = MockRouter()
        mockPostDetailsCacheService = MockPostDetailsCacheService(try! Realm())
        mockInteractor = MockPostDetailsInteractor()
        mockHanlder = MockHandler()
        mockPostDetailsNetworkService = MockPostDetailsNetworkService(mockInteractor , handler: mockHanlder)
        mockPresenter = MockPostDetailsPresenter(post: post, router: mockRouter)
        interactor = PostDetailsInteractor()
    }

    override func tearDownWithError() throws {
        mockRouter = nil
        post = nil
        mockPostDetailsCacheService = nil
        mockPostDetailsNetworkService = nil
        mockPresenter = nil
        interactor = nil
        mockInteractor  = nil
        mockHanlder = nil
    }

    func testSetNetworkService() throws {
        
//        interactor.setNetworkService(mockPostDetailsNetworkService)
//        XCTAssertNotNil(interactor.serviceNetwork)
//        XCTAssertTrue(interactor.serviceNetwork === mockPostDetailsNetworkService)
    }
    
    func testSetCacheService() throws {
        
//        interactor.setCacheService(mockPostDetailsCacheService)
//        XCTAssertNotNil(interactor.serviceCache)
//        XCTAssertTrue(interactor.serviceCache === mockPostDetailsCacheService)
    }
    
    func testSetPresenter() throws {
        
//        interactor.setPresenter(mockPresenter)
//        XCTAssertNotNil(interactor.presenter)
//        XCTAssertTrue(interactor.presenter === mockPresenter)
    }
}
