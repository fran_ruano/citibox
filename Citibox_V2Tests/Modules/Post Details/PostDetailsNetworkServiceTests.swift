//
//  Created by Fran Ruano on 21/1/21.
//

import XCTest
@testable import Citibox_V2
import Alamofire
import OHHTTPStubs

class PostDetailsNetworkServiceTests: XCTestCase {

    var service: PostDetailsNetworkService!
    var mockInteractor: MockPostDetailsInteractor!
    let testHost = "jsonplaceholder.typicode.com"
    
    override func setUpWithError() throws {
        mockInteractor = MockPostDetailsInteractor()
        service = PostDetailsNetworkService(mockInteractor, handler: Alamofire.AF)
        
        HTTPStubs.onStubActivation { (request: URLRequest, stub: HTTPStubsDescriptor, response: HTTPStubsResponse) in
                    print("*** [OHHTTPStubs] Request to \(request.url!) has been stubbed with \(String(describing: stub.name)) ***")
                }
    }

    override func tearDownWithError() throws {
        mockInteractor = nil
        service = nil
        HTTPStubs.removeAllStubs()
    }

    func testRequestUserDetailsSuccess() {
        
        stub(condition: isHost(testHost) && isPath("/users") && isMethodGET()) { req in
            guard let path = OHPathForFile("response_user.json", type(of: self)) else {
                preconditionFailure("*** Could not find expected file in test bundle ***")
            }
            return fixture(filePath: path,
                           status: 200,
                           headers: [ "Content-Type": "application/json" ])
        }
        
        let predicate = NSPredicate(block: { [self] any, _ in
            return mockInteractor.responseDetailsCalled.count > 0
        })
        
        let exp = expectation(for: predicate, evaluatedWith: self, handler: nil)
        
        service.requestDetails(id: 1)
        
        wait(for: [exp], timeout: 10.0)
        XCTAssertEqual(mockInteractor.responseDetailsCalled.count, 1)
        if case let .success(data) = mockInteractor.responseDetailsCalled.first {
            XCTAssertEqual(data.name, "Leanne Graham")
        } else  {
            XCTFail()
        }
        
    }
    
    func testRequestUSerDetailsError() {
        
        stub(condition: isHost(testHost) && isMethodGET()) { req in
                
            return HTTPStubsResponse(error: NetworkError.badURL)
            }
        
        let predicate = NSPredicate(block: { [self] any, _ in
            return mockInteractor.responseDetailsCalled.count > 0
          })
        
        let exp = expectation(for: predicate, evaluatedWith: self, handler: nil)

        service.requestDetails(id: 1)

        wait(for: [exp], timeout: 10.0)
        XCTAssertEqual(mockInteractor.responseDetailsCalled.count, 1)
        if case let .failure(error) = mockInteractor.responseDetailsCalled.first {
            XCTAssertNotNil(error)
        } else  {
            XCTFail()
        }

    }
}
