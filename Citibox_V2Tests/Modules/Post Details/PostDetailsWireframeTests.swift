//
//  Created by Fran Ruano on 19/1/21.
//
@testable import Citibox_V2

import XCTest

class PostDetailsWireframeTests: XCTestCase {

    var view: PostDetailsViewController!
    var mockRouter: MockRouter!
    var post: Post!
    
    override func setUpWithError() throws {
        view = PostDetailsViewController()
        mockRouter = MockRouter()
        post = Post(1)
    }

    override func tearDownWithError() throws {
        view =  nil
        mockRouter = nil
        post = nil
    }

    func testPrepare() throws {
        
        PostDetailsWireframe.preparePostDetailsView(view, post: post, router: mockRouter)
        
//        XCTAssertNotNil(view)
//        XCTAssertNotNil(view.presenter)
//        let presenter = view.presenter as! PostDetailsPresenter
//        XCTAssertNotNil(presenter.interactor)
//        let interactor = presenter.interactor as! PostDetailsInteractor
//        XCTAssertNotNil(interactor.serviceCache)
//        XCTAssertNotNil(interactor.serviceNetwork)
    }
}
