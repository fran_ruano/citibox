//
//  Created by Fran Ruano on 21/1/21.
//

import Foundation
@testable import Citibox_V2

class MockPostCommentsView: PostCommentsViewProtocol {
    func showComments(items: [CommentViewItem]?) {
        
    }
    
    
    func setPresenter(_ presenter: PostCommentsPresenterProtocol) {
    }
}
