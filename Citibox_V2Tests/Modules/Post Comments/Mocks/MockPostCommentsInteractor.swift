//
//  Created by Fran Ruano on 21/1/21.
//

import Foundation
@testable import Citibox_V2

class MockPostCommentsInteractor: PostCommentsInteractorProtocol {
    func requestComments(id: Int) {
        
    }
    
    func responseComments(_ response: Result<[JsonComment], Error>) {
        
    }
    
    func setPresenter(_ presenter: PostCommonPresenterProtocol) {
        
    }
    
    func setNetworkService(_ service: PostCommentsNetworkServiceProtocol) {
        
    }
    
    func setCacheService(_ service: PostCommentsCacheServiceProtocol) {
        
    }
    
    func setPresenter(_ presenter: PostCommentsPresenterProtocol) {
        
    }
}
