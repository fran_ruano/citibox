//
//  Created by Fran Ruano on 21/1/21.
//

import Foundation
@testable import Citibox_V2

class MockPostCommentsPresenter: PostCommentsPresenterProtocol {
    required init(_ comments: [Comment]) {
        
    }
    
    func requestComments() {
        
    }
    
    func setView(_ view: PostCommentsViewProtocol) {
        
    }
    
    func setInteractor(_ interactor: PostCommentsInteractorProtocol) {
        
    }
    
    
}
