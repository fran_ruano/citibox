//
//  Created by Fran Ruano on 21/1/21.
//

import XCTest
@testable import Citibox_V2

class PostCommentsPresenterTests: XCTestCase {

    var mockPostCommentsInteractor: MockPostCommentsInteractor!
    var mockPostCommentsView: MockPostCommentsView!
    var presenter: PostCommentsPresenter!
    
    override func setUpWithError() throws {
        
        mockPostCommentsInteractor = MockPostCommentsInteractor()
        mockPostCommentsView = MockPostCommentsView()
        presenter = PostCommentsPresenter([Comment]())
    }

    override func tearDownWithError() throws {
        
        mockPostCommentsInteractor = nil
        mockPostCommentsView = nil
        presenter = nil
    }

    func testInit() throws {
//        XCTAssertNotNil(presenter)
//        XCTAssertNil(presenter.view)
//        XCTAssertNil(presenter.interactor)
    }
    
    func testSetView() {
//        presenter.setView(mockPostCommentsView)
//        XCTAssertNotNil(presenter.view)
//        XCTAssertTrue(presenter.view === mockPostCommentsView)
    }
    
    func testSetInteractor() {
//        presenter.setInteractor(mockPostCommentsInteractor)
//        XCTAssertNotNil(presenter.interactor)
//        XCTAssertTrue(presenter.interactor === mockPostCommentsInteractor)
    }
}
