//
//  Created by Fran Ruano on 19/1/21.
//
@testable import Citibox_V2

import XCTest

class PostCommentsWireframeTests: XCTestCase {

    var view: PostCommentsViewController!
    
    override func setUpWithError() throws {
        view = PostCommentsViewController()
    }

    override func tearDownWithError() throws {
        view =  nil
    }

    func testPrepare() throws {
        
        PostCommentsWireframe.preparePostCommentsView(view, comments: [Comment]())
        
//        XCTAssertNotNil(view)
//        XCTAssertNotNil(view.presenter)
//        let presenter = view.presenter as! PostCommentsPresenter
//        XCTAssertNotNil(presenter.interactor)
//        let interactor = presenter.interactor as! PostCommentsInteractor
//        XCTAssertNotNil(interactor.serviceCache)
//        XCTAssertNotNil(interactor.serviceNetwork)
    }
}
