//
//  Created by Fran Ruano on 21/1/21.
//

import XCTest
@testable import Citibox_V2
import Alamofire
import RealmSwift

class MockPostCommentsCacheService: PostCommentsCacheServiceProtocol {
    func getCommentsByPostId(_ id: Int) -> [Comment]? {
        
        return nil
    }
    
    var storage: Realm
    
    required init(_ storage: Realm) {
        self.storage = storage
    }
}

class MockPostCommentsNetworkService: PostCommentsNetworkServiceProtocol {
    func requestComments(id: Int) {
        
    }
    
    required init(_ interactor: PostCommentsInteractorProtocol, handler: Session) {
        
    }

}

class PostCommentsInteractorTests: XCTestCase {

    var mockPostCommentsCacheService: MockPostCommentsCacheService!
    var mockPostCommentsNetworkService: MockPostCommentsNetworkService!
    var mockPresenter: MockPostCommentsPresenter!
    var interactor: PostCommentsInteractor!
    var mockInteractor: MockPostCommentsInteractor!
    var mockHandler: MockHandler!
    
    override func setUpWithError() throws {
        mockPostCommentsCacheService = MockPostCommentsCacheService(try! Realm())
        mockInteractor = MockPostCommentsInteractor()
        mockHandler = MockHandler()
        mockPostCommentsNetworkService = MockPostCommentsNetworkService(mockInteractor, handler: mockHandler)
        mockPresenter = MockPostCommentsPresenter([Comment]())
        interactor = PostCommentsInteractor()
    }

    override func tearDownWithError() throws {
        
        mockInteractor = nil
        mockPostCommentsCacheService = nil
        mockPostCommentsNetworkService = nil
        mockPresenter = nil
        interactor = nil
        mockHandler = nil
    }

    func testSetNetworkService() throws {
        
//        interactor.setNetworkService(mockPostCommentsNetworkService)
//        XCTAssertNotNil(interactor.serviceNetwork)
//        XCTAssertTrue(interactor.serviceNetwork === mockPostCommentsNetworkService)
    }
    
    func testSetCacheService() throws {
        
//        interactor.setCacheService(mockPostCommentsCacheService)
//        XCTAssertNotNil(interactor.serviceCache)
//        XCTAssertTrue(interactor.serviceCache === mockPostCommentsCacheService)
    }
    
    func testSetPresenter() throws {
        
//        interactor.setPresenter(mockPresenter)
//        XCTAssertNotNil(interactor.presenter)
//        XCTAssertTrue(interactor.presenter === mockPresenter)
    }
}
