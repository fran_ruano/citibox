//
//  Created by Fran Ruano on 19/1/21.
//

import XCTest
@testable import Citibox_V2

class PostListViewControllerTests: XCTestCase {

    var mockPostListPresenter: MockPostListPresenter!
    var controller: PostListViewController!
    var mockRouter: MockRouter!
    
    override func setUpWithError() throws {

        mockRouter = MockRouter()
        mockPostListPresenter = MockPostListPresenter(mockRouter)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        controller = storyboard.instantiateViewController(withIdentifier: "PostListID") as? PostListViewController
        _ = controller.view
    }

    override func tearDownWithError() throws {

        mockRouter = nil
        mockPostListPresenter = nil
        controller = nil
    }

    func testSetPresenter() throws {

        controller.setPresenter(mockPostListPresenter)
        XCTAssertNotNil(controller.presenter)
        XCTAssertTrue(controller.presenter === mockPostListPresenter)
    }

    func testDidAppear() throws {
        controller.viewDidAppear(false)
        XCTAssertNotNil(controller.tableView.tableFooterView)
        XCTAssertEqual(controller.refreshControl, controller.tableView.refreshControl)
    }
    
    func testDidLoad() throws {
        
        XCTAssertTrue(controller.tableView.dataSource === controller)
    }
}
