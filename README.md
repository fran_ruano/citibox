# README #

This is the implementation of Citibox tech test.

### What do you need to know? ###

###### Quick summary ######
In this implementation I decided to implement VIPER to show a very efficient layer separation, to implement a clean architecture. 
The app includes a cache that get cleaned when the user drag and pull the post list. Also, the post list includes and infinite scrolling adding more posts when
the user reach the bottom of the screen.

###### Limitations ######

- Infinite scrolling

The infinite scrolling will keep sending requests to the network layer even if already received the las post. This implementation would be useful in cases when 
posts get updated quite often. 

- Network request

When we request user details and comments the app doesn't display any loading indicator. Even when it could be a good practice, I don't think it's 100% necessary
in this case as we already have information on the screen

- Unit testing

Because of time limitations I haven't developed the app using TDD or fully unit tested. However, the app is 100% ready to be tested and it's already tested 
over 55% of the code with relevant tests.
"Post List" module is probably the section of the app with the best test coverage and it could be a good example of how the app could be tested.

- Errors

We don't provide any error information to the user.




###### Personal opinion ######
Even VIPER could sounds like the ultimate architecture as it's easily testable and it has a clear layer separation, I have certain concerns about it.

On the positive side, VIPER has a very clear layer separation and it provides a template to scale the app easily without important changes in the app.

On the not as positive side.

First of all, it's an architecture that requires a lot of boiler plate code and quite a lot of self discipline to test properly. In small teams where speed
of development is key, could be very problematic as cutting corners could lead to a very complicated code to maintain.

Aditionally, it needs a very clear idea of the product that you're trying to build. Start-ups where the concept tend to pivot quite often could make extremely 
difficult to keep a clean development.

Finally, it requires certain level of seniority to implement this architecture. Different people think diferently about how to implement VIPER and it needs a level 
of experience to do it in a "correct" way.