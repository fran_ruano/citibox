//
//  Created by Fran Ruano on 26/1/21.
//

import Foundation

struct JsonUser: Codable {
    let id: Int
    let name, username, email: String
    let address: JsonAddress?
    let phone, website: String?
    let company: JsonCompany?
}

// MARK: - Address
struct JsonAddress: Codable {
    let street, suite, city, zipcode: String?
    let geo: JsonGeo?
}

// MARK: - Geo
struct JsonGeo: Codable {
    let lat, lng: String
}

// MARK: - Company
struct JsonCompany: Codable {
    let name, catchPhrase, bs: String?
}
