//
//  Created by Fran Ruano on 26/1/21.
//

import Foundation
import RealmSwift

class User: Object {

    @objc dynamic var id: Int = -1
    @objc dynamic var name: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var username: String = ""
    
    convenience init(_ json: JsonUser) {
        self.init()
    
        id = json.id
        name = json.name
        email = json.email
        username = json.username
    }
    
    override static func primaryKey() -> String? {
       return "id"
     }
}
