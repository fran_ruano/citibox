//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation
import RealmSwift

protocol PostDetailsCacheServiceProtocol: CacheService {
    
    func getUserById(_ id: Int) -> User?
}

class PostDetailsCacheService: PostDetailsCacheServiceProtocol {
    
    let storage: Realm
    
    required init(_ storage: Realm) {
        self.storage = storage
    }
    
    func getUserById(_ id: Int) -> User? {
        let user = storage.objects(User.self).filter("id == \(id)").first
        
        return user
    }
}
