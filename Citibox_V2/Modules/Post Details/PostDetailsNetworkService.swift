//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation
import Alamofire

protocol PostDetailsNetworkServiceProtocol: class {
    
    init(_ interactor: PostDetailsInteractorProtocol, handler: Alamofire.Session)
    func requestDetails(id: Int)
}

class PostDetailsNetworkService: PostDetailsNetworkServiceProtocol, NetworkService {
    
    private(set) weak var interactor: PostDetailsInteractorProtocol?
    private(set) var handler: Alamofire.Session
    
    required init(_ interactor: PostDetailsInteractorProtocol, handler: Session) {
        
        self.interactor = interactor
        self.handler = handler
    }
    
    func requestDetails(id: Int) {
        guard let url = URL(string: NetworkURL.userDetails(id)) else {
            interactor?.responseDetails(.failure(NetworkError.badURL))
            return
        }
        request(url: url, type: JsonUser.self) { [weak self] details in
            self?.interactor?.responseDetails(.success(details))
        } failure: { [weak self] error in
            self?.interactor?.responseDetails(.failure(error as NSError))
        }
    }
}
