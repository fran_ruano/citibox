//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation

protocol PostDetailsInteractorProtocol: class {
    
    func setNetworkService(_ service: PostDetailsNetworkServiceProtocol)
    func setCacheService(_ service: PostDetailsCacheServiceProtocol)
    func setPresenter(_ presenter: PostDetailsPresenterProtocol)
    
    func requestDetails(id: Int)
    func responseDetails(_ response: Result<JsonUser, Error>)
}

class PostDetailsInteractor: PostDetailsInteractorProtocol {
    
    private(set) weak var presenter: PostDetailsPresenterProtocol?
    private(set) var serviceCache: PostDetailsCacheServiceProtocol?
    private(set) var serviceNetwork: PostDetailsNetworkServiceProtocol?
    
    func setNetworkService(_ service: PostDetailsNetworkServiceProtocol) {
        
        serviceNetwork = service
    }
    
    func setCacheService(_ service: PostDetailsCacheServiceProtocol) {
        
        serviceCache = service
    }
    
    func setPresenter(_ presenter: PostDetailsPresenterProtocol) {
        
        self.presenter = presenter
    }
    
    func requestDetails(id: Int) {
        
        let user = serviceCache?.getUserById(id)
        if let user = user {
            presenter?.responseUser(user)
        } else {
            serviceNetwork?.requestDetails(id: id)
        }
    }
    
    func responseDetails(_ response: Result<JsonUser, Error>) {
        switch response {
        case .failure(_):
//            Should propagate error
            break
        case .success(let jsonUser):
            let user = User(jsonUser)
            serviceCache?.save(user)
            presenter?.responseUser(user)
        }
    }
}
