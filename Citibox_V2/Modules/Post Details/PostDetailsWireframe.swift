//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation
import Alamofire
import RealmSwift

class PostDetailsWireframe {
    
    static func preparePostDetailsView(_ view: PostDetailsViewProtocol, post: Post, router: Router) {

        guard let realm = try? Realm() else { return }
        
        let presenter: PostDetailsPresenterProtocol = PostDetailsPresenter(post: post, router: router)
        let interactorDetails: PostDetailsInteractorProtocol = PostDetailsInteractor()
        let serviceNetwork: PostDetailsNetworkServiceProtocol = PostDetailsNetworkService(interactorDetails, handler: Alamofire.AF)
        let serviceCache: PostDetailsCacheServiceProtocol = PostDetailsCacheService(realm)
        
        interactorDetails.setPresenter(presenter)
        interactorDetails.setNetworkService(serviceNetwork)
        interactorDetails.setCacheService(serviceCache)
        presenter.setInteractor(interactorDetails)
        
        let interactorComments: PostCommentsInteractorProtocol = PostCommentsInteractor()
        let serviceNetworkComments: PostCommentsNetworkServiceProtocol = PostCommentsNetworkService(interactorComments, handler: Alamofire.AF)
        let serviceCacheComments: PostCommentsCacheServiceProtocol = PostCommentsCacheService(realm)

        if let _ = presenter as? PostCommonPresenterProtocol {
            interactorComments.setPresenter(presenter as! PostCommonPresenterProtocol)
            interactorComments.setNetworkService(serviceNetworkComments)
            interactorComments.setCacheService(serviceCacheComments)
            presenter.setInteractor(interactorComments)
        }
        
        presenter.setView(view)
        view.setPresenter(presenter)

    }
}
