//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation

protocol PostDetailsPresenterProtocol: class {
    
    func setView(_ view: PostDetailsViewProtocol)
    func setInteractor(_ interactor: PostDetailsInteractorProtocol)
    func setInteractor(_ interactor: PostCommentsInteractorProtocol)
    init(post: Post, router: Router)
    
    func requestDetails()
    func responseUser(_ user: User)
    func openComments()
}

class PostDetailsPresenter: PostDetailsPresenterProtocol, PostCommonPresenterProtocol {
    
    let router: Router
    let post: Post
    private(set) var comments: [Comment]?
    
    private(set) weak var view: PostDetailsViewProtocol?
    private(set) var interactorDetails: PostDetailsInteractorProtocol?
    private(set) var interactorComments: PostCommentsInteractorProtocol?
    
    required init(post: Post, router: Router) {
        
        self.router = router
        self.post = post
    }
    
    func setView(_ view: PostDetailsViewProtocol) {

        self.view = view
    }

    func setInteractor(_ interactor: PostDetailsInteractorProtocol) {

        self.interactorDetails = interactor
    }
    
    func setInteractor(_ interactor: PostCommentsInteractorProtocol) {

        self.interactorComments = interactor
    }
    
    func requestDetails() {
        
        let item = PostViewItem(title: post.title, subtitle: post.body)
        view?.setItem(item)
        interactorDetails?.requestDetails(id: post.userID)
        interactorComments?.requestComments(id: post.id)
    }
    
    func responseUser(_ user: User) {
        let userName = user.name
        DispatchQueue.main.async {
            self.view?.setUserName(userName)
        }
    }
    
    func responseComments(_ comments: [Comment]) {
        
        self.comments = comments
        let commentsCount = comments.count
        var message = ""
        switch commentsCount {
        case 0:
            message = "No comments yet"
        case 1:
            message = "\(commentsCount) comment"
        default:
            message = "\(commentsCount) comments"
        }
        DispatchQueue.main.async {
            self.view?.setComments(message)
        }
    }
    
    func openComments() {
        if let comments = self.comments,
           comments.count > 0 {
            
            router.moveToComments(comments)
        }
    }
}
