//
//  Created by Fran Ruano on 26/1/21.
//

import UIKit

class Router {
    
    private(set) var navigationStack: UINavigationController?
    
    func getInitialNavController() -> UINavigationController? {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateViewController(identifier: "PostListID") as? PostListViewController {
            PostListWireframe.preparePostListView(controller, router: self)
            navigationStack = UINavigationController(rootViewController: controller)
        }
        return navigationStack
    }
    
    func moveToPost(_ post: Post) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateViewController(identifier: "PostDetailsID") as? PostDetailsViewController {
            PostDetailsWireframe.preparePostDetailsView(controller, post: post, router: self)
            
            navigationStack?.pushViewController(controller,
                                                animated: true)
        }
    }
    
    func moveToComments(_ comments: [Comment]) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateViewController(identifier: "PostCommentsID") as? PostCommentsViewController {
            PostCommentsWireframe.preparePostCommentsView(controller, comments: comments)

            navigationStack?.pushViewController(controller,
                                                animated: true)
        }
    }
}
