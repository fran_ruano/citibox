//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation

protocol PostCommentsInteractorProtocol: class {
    
    func setNetworkService(_ service: PostCommentsNetworkServiceProtocol)
    func setCacheService(_ service: PostCommentsCacheServiceProtocol)
    func setPresenter(_ presenter: PostCommonPresenterProtocol)
    
    func requestComments(id: Int)
    func responseComments(_ response: Result<[JsonComment], Error>)
}


class PostCommentsInteractor: PostCommentsInteractorProtocol {
    
    private(set) weak var presenter: PostCommonPresenterProtocol?
    private(set) var serviceCache: PostCommentsCacheServiceProtocol?
    private(set) var serviceNetwork: PostCommentsNetworkServiceProtocol?
    
    func setNetworkService(_ service: PostCommentsNetworkServiceProtocol) {
        
        serviceNetwork = service
    }
    
    func setCacheService(_ service: PostCommentsCacheServiceProtocol) {
        
        serviceCache = service
    }
    
    func setPresenter(_ presenter: PostCommonPresenterProtocol) {
        
        self.presenter = presenter
    }
    
    func requestComments(id: Int) {
        
        let comments = serviceCache?.getCommentsByPostId(id)
        if let comments = comments, comments.count > 0 {
            presenter?.responseComments(comments)
        } else {
            serviceNetwork?.requestComments(id: id)
        }
    }
    
    func responseComments(_ response: Result<[JsonComment], Error>) {
        switch response {
        case .failure(_):
//            Should propagate error
            break
        case .success(let jsonComments):
            let comments = jsonComments.map { json -> Comment in
                return Comment(json)
            }
            serviceCache?.save(comments)
            presenter?.responseComments(comments)
        }
    }
}
