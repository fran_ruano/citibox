//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation
import Alamofire

protocol PostCommentsNetworkServiceProtocol: class {
    
    init(_ interactor: PostCommentsInteractorProtocol, handler: Alamofire.Session)
    func requestComments(id: Int)
}

class PostCommentsNetworkService: PostCommentsNetworkServiceProtocol, NetworkService {
    
    private(set) weak var interactor: PostCommentsInteractorProtocol?
    let handler: Alamofire.Session
    
    required init(_ interactor: PostCommentsInteractorProtocol, handler: Session) {
        
        self.interactor = interactor
        self.handler = handler
    }
    
    func requestComments(id: Int) {
        guard let url = URL(string: NetworkURL.comments(id: id)) else {
            
            interactor?.responseComments(.failure(NetworkError.badURL))
            return
        }
        request(url: url, type: [JsonComment].self) { [weak self] comments in
            self?.interactor?.responseComments(.success(comments))
        } failure: { [weak self] error in
            self?.interactor?.responseComments(.failure(error as NSError))
        }
    }
}
