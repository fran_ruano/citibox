//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation

protocol PostCommonPresenterProtocol: class {
    
    func setInteractor(_ interactor: PostCommentsInteractorProtocol)
    func responseComments(_ comments: [Comment])
}

protocol PostCommentsPresenterProtocol: class {
    
    init(_ comments: [Comment])
    func setView(_ view: PostCommentsViewProtocol)
    
    func requestComments()
}

class PostCommentsPresenter: PostCommentsPresenterProtocol, PostCommonPresenterProtocol {
    
    private(set) weak var view: PostCommentsViewProtocol?
    private(set) var interactor: PostCommentsInteractorProtocol?
    private(set) var comments: [Comment]?

    required init(_ comments: [Comment]) {
    
        self.comments = comments
    }
    
    func setView(_ view: PostCommentsViewProtocol) {

        self.view = view
    }

    func setInteractor(_ interactor: PostCommentsInteractorProtocol) {

        self.interactor = interactor
    }
    
    func responseComments(_ comments: [Comment]) {
        
        self.comments = comments
    }
    
    func requestComments() {
        
        let items = comments?.map { comment -> CommentViewItem in
                return CommentViewItem(comment)
        }
        DispatchQueue.main.async {
            self.view?.showComments(items: items)
        }
    }
}
