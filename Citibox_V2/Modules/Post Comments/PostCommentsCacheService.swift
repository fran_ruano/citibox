//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation
import RealmSwift

protocol PostCommentsCacheServiceProtocol: CacheService {
    
    func getCommentsByPostId(_ id: Int) -> [Comment]?
}

class PostCommentsCacheService: PostCommentsCacheServiceProtocol {
    
    let storage: Realm
    
    required init(_ storage: Realm) {
        self.storage = storage
    }
    
    func getCommentsByPostId(_ id: Int) -> [Comment]? {
        let comments = storage.objects(Comment.self).filter("postID == \(id)")
        
        return Array(comments)
    }
}
