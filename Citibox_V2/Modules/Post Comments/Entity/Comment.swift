//
//  Created by Fran Ruano on 27/1/21.
//

import Foundation
import RealmSwift

class Comment: Object {
    
    @objc dynamic var id: Int = -1
    @objc dynamic var postID: Int = -1
    @objc dynamic var name: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var body: String = ""
    
    convenience init(_ json: JsonComment) {
        self.init()
    
        id = json.id
        postID = json.postID
        name = json.name
        email = json.email
        body = json.body
    }
    
    override static func primaryKey() -> String? {
       return "id"
     }
}

