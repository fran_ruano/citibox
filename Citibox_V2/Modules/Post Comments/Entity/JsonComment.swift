//
//  Created by Fran Ruano on 27/1/21.
//

import Foundation

// MARK: - JSONComment
struct JsonComment: Codable {
    let postID, id: Int
    let name, email, body: String

    enum CodingKeys: String, CodingKey {
        case postID = "postId"
        case id, name, email, body
    }
}
