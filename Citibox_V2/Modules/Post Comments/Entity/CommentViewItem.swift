//
//  Created by Fran Ruano on 27/1/21.
//

import Foundation

struct CommentViewItem: EmailEmoji {
    
    let title: String
    let body: String
    let email: String
    
    var emojiEmail: String {
        get {
            return getEmoji(email)
        }
    }
    
    init(_ comment: Comment) {
        title = comment.name
        body = comment.body
        email = comment.email
    }
}
