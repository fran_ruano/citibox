//
//  Created by Fran Ruano on 7/1/21.
//

import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    func setData(_ comment: CommentViewItem?) {
        lblTitle.text = comment?.title
        lblComment.text = comment?.body
        lblEmail.text = comment?.emojiEmail
    }
}
