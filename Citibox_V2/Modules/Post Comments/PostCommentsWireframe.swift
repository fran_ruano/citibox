//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation


class PostCommentsWireframe {
    
    static func preparePostCommentsView(_ view: PostCommentsViewProtocol, comments: [Comment]) {

        let presenter: PostCommentsPresenterProtocol = PostCommentsPresenter(comments)

        presenter.setView(view)
        view.setPresenter(presenter)

    }
}
