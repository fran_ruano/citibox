//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation

protocol PostListPresenterProtocol: class {
    
    init(_ router: Router)
    func setView(_ view: PostListViewProtocol)
    func setInteractor(_ interactor: PostListInteractorProtocol)
    
    func requestPosts(reload: Bool)
    func responsePosts(_ posts: [Post])
    func selectRow(_ index: Int)
    func updatePage(_ postCount: Int)
}

class PostListPresenter: PostListPresenterProtocol {
    
    let router: Router
    private(set) weak var view: PostListViewProtocol?
    private(set) var interactor: PostListInteractorProtocol?
    
    private(set) var currentPage = 0
    private(set) var posts = [Post]()
    
    required init(_ router: Router) {
        self.router = router
    }
    
    func setView(_ view: PostListViewProtocol) {
        
        self.view = view
    }
    
    func setInteractor(_ interactor: PostListInteractorProtocol) {
        
        self.interactor = interactor
    }
    
    func requestPosts(reload: Bool = false) {
        if reload {
            currentPage = 0
            interactor?.forceRequestPost(page: currentPage)
        } else {
            interactor?.requestPosts(page: currentPage)
        }
        currentPage += 1
    }
    
    func responsePosts(_ posts: [Post]) {
        let items = posts.map { (post) -> PostViewItem in
            return PostViewItem(title: post.title, subtitle: post.body)
        }
        
        let isReload = currentPage == 1
        if isReload{
            self.posts = posts
        } else {
            self.posts.append(contentsOf: posts)
        }
        DispatchQueue.main.async {
            self.view?.responsePosts(items, reload: isReload)
        }
    }
    
    func selectRow(_ index: Int) {
        
        let post = posts[index]
        router.moveToPost(post)
    }
    
    func updatePage(_ postCount: Int) {
        let newPage = Int(postCount / Constants.postListSize)
        currentPage = newPage
    }
}
