//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation
import Alamofire
import RealmSwift

class PostListWireframe {
    
    static func preparePostListView(_ view: PostListViewProtocol, router: Router) {

        guard let realm = try? Realm() else { return }
        let presenter: PostListPresenterProtocol = PostListPresenter(router)
        let interactor: PostListInteractorProtocol = PostListInteractor()
        let serviceNetwork: PostListNetworkServiceProtocol =  PostListNetworkService(interactor, handler: Alamofire.AF)
        let serviceCache: PostListCacheServiceProtocol =  PostListCacheService(realm)

        interactor.setPresenter(presenter)
        interactor.setNetworkService(serviceNetwork)
        interactor.setCacheService(serviceCache)
        presenter.setInteractor(interactor)
        presenter.setView(view)
        view.setPresenter(presenter)

    }
}
