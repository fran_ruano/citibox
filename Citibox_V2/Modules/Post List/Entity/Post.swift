//
//  Created by Fran Ruano on 2/1/21.
//

import Foundation
import RealmSwift

class Post: Object {

    @objc dynamic var title: String = ""
    @objc dynamic var body: String = ""
    @objc dynamic var id: Int = -1
    @objc dynamic var userID: Int = 0
    
    convenience init(_ json: JsonPost) {
        self.init()
        
        title = json.title
        body = json.body
        id = json.id
        userID = json.userID
    }
    
    // Facilitate unit test
    convenience init(_ id: Int) {
        self.init()
        
        self.id = id
    }
    
    override static func primaryKey() -> String? {
       return "id"
     }
}
