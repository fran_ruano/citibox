//
//  Created by Fran Ruano on 22/1/21.
//

import Foundation

struct PostViewItem {
    let title: String
    let subtitle: String
}
