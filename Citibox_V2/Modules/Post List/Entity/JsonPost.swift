//
//  Created by Fran Ruano on 1/1/21.
//

import Foundation

// MARK: - JSONPost
struct JsonPost: Codable {
    let userID, id: Int
    let title, body: String

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}
