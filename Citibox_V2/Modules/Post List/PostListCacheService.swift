//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation
import RealmSwift

protocol PostListCacheServiceProtocol: CacheService {
    
}

class PostListCacheService: PostListCacheServiceProtocol {
    let storage: Realm
    
    required init(_ storage: Realm) {
        self.storage = storage
    }
}
