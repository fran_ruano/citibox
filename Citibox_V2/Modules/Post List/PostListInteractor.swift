//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation

protocol PostListInteractorProtocol: class {
    
    func setNetworkService(_ service: PostListNetworkServiceProtocol)
    func setCacheService(_ service: PostListCacheServiceProtocol)
    func setPresenter(_ presenter: PostListPresenterProtocol)
    
    func requestPosts(page: Int)
    func forceRequestPost(page: Int)
    func responsePosts(_ response: Result<[JsonPost], Error>)
}

class PostListInteractor: PostListInteractorProtocol {
    
    private(set) weak var presenter: PostListPresenterProtocol?
    private(set) var serviceCache: PostListCacheServiceProtocol?
    private(set) var serviceNetwork: PostListNetworkServiceProtocol?
    
    func setNetworkService(_ service: PostListNetworkServiceProtocol) {
        
        serviceNetwork = service
    }
    
    func setCacheService(_ service: PostListCacheServiceProtocol) {
        
        serviceCache = service
    }
    
    func setPresenter(_ presenter: PostListPresenterProtocol) {
        
        self.presenter = presenter
    }
    
    func forceRequestPost(page: Int) {
        serviceCache?.deleteAll()
        serviceNetwork?.requestPosts(page: page)
    }
    
    func requestPosts(page: Int) {
        
        if page > 0 {
            serviceNetwork?.requestPosts(page: page)
            return
        }
        
        let posts = serviceCache?.getObjects(Post.self)
        if let posts = posts,
           posts.count > 0 {
            presenter?.responsePosts(posts)
            presenter?.updatePage(posts.count)
        } else {
            serviceNetwork?.requestPosts(page: page)
        }
    }
    
    func responsePosts(_ response: Result<[JsonPost], Error>) {
        switch response {
        case .failure(_):
//            Should propagate error
            break
        case .success(let jsonPosts):
            let posts = jsonPosts.map { jsonPost -> Post in
                return Post(jsonPost)
            }
            serviceCache?.save(posts)
            presenter?.responsePosts(posts)
        }
    }
}
