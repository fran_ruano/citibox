//
//  Created by Fran Ruano on 19/1/21.
//

import Foundation
import Alamofire

protocol PostListNetworkServiceProtocol: class {

    init(_ interactor: PostListInteractorProtocol, handler: Alamofire.Session)
    func requestPosts(page: Int)
}

class PostListNetworkService: PostListNetworkServiceProtocol, NetworkService {
    
    private(set) weak var interactor: PostListInteractorProtocol?
    private(set) var handler: Alamofire.Session
    
    required init(_ interactor: PostListInteractorProtocol, handler: Alamofire.Session) {
        self.interactor = interactor
        self.handler = handler
    }
    
    func requestPosts(page: Int) {
        guard let url = URL(string: NetworkURL.post(page: page)) else {
            
            interactor?.responsePosts(.failure(NetworkError.badURL))
            return
        }
        request(url: url, type: [JsonPost].self) { [weak self] posts in
            self?.interactor?.responsePosts(.success(posts))
        } failure: { [weak self] error in
            self?.interactor?.responsePosts(.failure(error as NSError))
        }

    }
}
