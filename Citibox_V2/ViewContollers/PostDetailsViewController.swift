//
//  Created by Fran Ruano on 19/1/21.
//

import UIKit

protocol PostDetailsViewProtocol: class {
    
    func setPresenter(_ presenter: PostDetailsPresenterProtocol)
    
    func setItem(_ item: PostViewItem)
    func setUserName(_ name: String)
    func setComments(_ name: String)
}

class PostDetailsViewController: UIViewController, PostDetailsViewProtocol {

    @IBOutlet weak var bodyTextView: UITextView!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    
    private(set) var presenter: PostDetailsPresenterProtocol?
    private(set) var item: PostViewItem? {
        didSet {
            title = item?.title
            bodyTextView.text = item?.subtitle
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.requestDetails()
        configUI()
    }
    
    func setPresenter(_ presenter: PostDetailsPresenterProtocol) {
        
        self.presenter = presenter
    }
    
    func setItem(_ item: PostViewItem) {
        
        self.item = item
    }
    
    func setUserName(_ name: String) {
        userLabel.text = name
        commentButton.isHidden = false
    }
    
    func setComments(_ name: String) {
        commentButton.setTitle(name, for: UIControl.State.normal)
        commentButton.isHidden = false
    }
    
    @IBAction func commentsButton_Pressed(_ sender: Any) {
        presenter?.openComments()
    }
    
}

extension PostDetailsViewController {
    
    private func configUI() {
        userLabel.text = ""
        commentButton.setTitle("", for: UIControl.State.normal)
        commentButton.isHidden = true
    }
}
