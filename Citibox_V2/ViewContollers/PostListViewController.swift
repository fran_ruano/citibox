//
//  Created by Fran Ruano on 19/1/21.
//

import UIKit

protocol PostListViewProtocol: class {
    
    func setPresenter(_ presenter: PostListPresenterProtocol)
    func responsePosts(_ posts: [PostViewItem], reload: Bool)
}

class PostListViewController: UIViewController, PostListViewProtocol {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            setupRefreshControl()
        }
    }
    private(set) var refreshControl: UIRefreshControl?
    
    private(set) var presenter: PostListPresenterProtocol?
    private(set) var items = [PostViewItem]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        presenter?.requestPosts(reload: true)
        showRefreshControlOnStart()
        tableView.tableFooterView = UIView()
    }
    
    func setPresenter(_ presenter: PostListPresenterProtocol) {
        self.presenter = presenter
    }
    
    func responsePosts(_ posts: [PostViewItem], reload: Bool) {
        if reload {
            self.items = posts
        } else {
            self.items.append(contentsOf: posts)
        }
        hideRefreshControl()
    }
    
    @objc func refresh(_ sender: AnyObject) {
        presenter?.requestPosts(reload: true)
    }
}

extension PostListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cell")
        cell.textLabel?.text = items[indexPath.row].title
        cell.detailTextLabel?.text = items[indexPath.row].subtitle
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = items.count - 1
        if indexPath.row == lastElement {
            presenter?.requestPosts(reload: false)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.selectRow(indexPath.row)
    }
}

extension PostListViewController {
    
    private func setupRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView?.refreshControl = refreshControl
    }
    
    private func showRefreshControlOnStart() {
        if let refreshControl = refreshControl {
            refreshControl.beginRefreshing()
            let contentOffset = CGPoint(x: 0, y: -refreshControl.frame.height)
            tableView.setContentOffset(contentOffset, animated: true)
        }
    }
    
    private func hideRefreshControl() {
        refreshControl?.endRefreshing()
    }
}

