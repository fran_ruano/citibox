//
//  Created by Fran Ruano on 19/1/21.
//

import UIKit

protocol PostCommentsViewProtocol: class {
    
    func setPresenter(_ presenter: PostCommentsPresenterProtocol)
    
    func showComments(items: [CommentViewItem]?)
}

class PostCommentsViewController: UIViewController, PostCommentsViewProtocol {

    @IBOutlet weak var tableView: UITableView!
    
    private(set) var presenter: PostCommentsPresenterProtocol?
    private(set) var items: [CommentViewItem]? {
        didSet {
            tableView?.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.dataSource = self
        presenter?.requestComments()
    }
    
    func setPresenter(_ presenter: PostCommentsPresenterProtocol) {
        
        self.presenter = presenter
    }
    
    func showComments(items: [CommentViewItem]?) {
        
        self.items = items
    }
}

extension PostCommentsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as? CommentCell else {
            return UITableViewCell()
        }
        cell.setData(items?[indexPath.row])
        
        return cell
    }
    
    
}
