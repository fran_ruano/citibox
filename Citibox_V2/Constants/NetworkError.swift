//
//  Created by Fran Ruano on 31/1/21.
//

import Foundation

enum NetworkError: Error {
    case badURL
    case unknown
}

struct NetworkURL {
    static let host = "https://jsonplaceholder.typicode.com"
    
    static func post(page: Int, limit: Int = Constants.postListSize) -> String {
        return "\(host)/posts?_page=\(page)&_limit=\(limit)"
    }
    
    static func userDetails(_ id: Int) -> String {
        return "\(host)/users/\(id)"
    }
    
    static func comments(id: Int) -> String {
        return "\(host)/posts/\(id)/comments"
    }
}
