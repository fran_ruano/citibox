//
//  SceneDelegate.swift
//  Citibox_V2
//
//  Created by Fran Ruano on 19/1/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    lazy var router = Router()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        if let windowScene = scene as? UIWindowScene {
            
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = router.getInitialNavController()
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}

