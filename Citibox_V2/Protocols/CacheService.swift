//
//  Created by Fran Ruano on 28/1/21.
//

import Foundation
import RealmSwift

protocol CacheService: class {
    
    var storage: Realm { get }
    
    init(_ storage: Realm)
    func save<T: Object>(_ objects: [T])
    func save<T: Object>(_ object: T)
    func getObjects<T: Object>(_ type: T.Type) -> [T]
    func deleteAll()
}

extension CacheService {
    

    public func save<T: Object>(_ objects: [T]) {
        do {
            try storage.write {
                storage.add(objects, update: Realm.UpdatePolicy.all)
            }
        } catch {
          // handle error
        }
    }

    public func save<T: Object>(_ object: T) {
        do {
            try storage.write {
                storage.add(object, update: Realm.UpdatePolicy.all)
            }
        } catch {
          // handle error
        }
    }

    public func getObjects<T: Object>(_ type: T.Type) -> [T] {
        let list = storage.objects(type)

        return Array(list)
    }
    
    public func deleteAll() {
        do {
            try storage.write {
                storage.deleteAll()
            }
        } catch {
          // handle error
        }
        
    }
}
