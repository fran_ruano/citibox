//
//  Created by Fran Ruano on 28/1/21.
//

import Foundation
import Alamofire

protocol NetworkService {
    var handler: Alamofire.Session { get }
    
    func request<T: Decodable>(url: URL,
                               type: T.Type,
                               success: @escaping ((T) -> Void),
                               failure: @escaping ((Error) -> Void))
}

extension NetworkService {
    
    func request<T: Decodable>(url: URL,
                               type: T.Type,
                               success: @escaping ((T) -> Void),
                               failure: @escaping ((Error) -> Void)) {
        
        let _ = handler.request(url)
            .validate()
            .responseDecodable(of: T.self) { response in
                switch(response.result) {
                case .failure(let error):
                    failure(error)
                case .success(let json):
                    success(json)
                }
            }
    }
}
