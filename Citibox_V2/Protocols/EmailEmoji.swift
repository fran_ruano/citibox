//
//  Created by Fran Ruano on 7/1/21.
//

import Foundation

protocol EmailEmoji {
    
    func getEmoji(_ text: String) -> String
}

extension EmailEmoji {
    
    func getEmoji(_ text: String) -> String {
        if text.hasSuffix(".info") {
            return "ℹ️ \(text)"
        }
        if text.hasSuffix(".co.uk") {
            return "🇬🇧 \(text)"
        }
        return "\(text)"
    }
}
